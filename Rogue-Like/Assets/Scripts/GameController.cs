﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController: MonoBehaviour {

    public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesmoving;
    public int playerCurrentHealth = 50;
    public AudioClip gameOverSound;

    private BoardController boardController;
    private List<Enemy> enemies;
    private GameObject levelImage;
    private Text levelText;
    private GameObject startScreen;
    private Text startText;
    private bool settingUpGame;
    private int secondsUntilLevelStart = 2;
    private int currentLevel = 1;

	void Awake () {
        if(Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
        boardController = GetComponent<BoardController>();
        enemies = new List<Enemy>();
	}

    void Start()
    {
        InitializeStartScreen();
    }
    
    private void InitializeStartScreen()
    {
        startScreen = GameObject.Find("Start Screen");
        startText = GameObject.Find("Start Text").GetComponent<Text>();
        startText.text = "Rogue Like";
        startScreen.SetActive(true);
        Invoke("DisableStartScreen", secondsUntilLevelStart);
    }

    private void InitializeGame()
    {
        if(startScreen != null)
        {
            startScreen.SetActive(false);
        }
        else
        {
            startScreen = GameObject.Find("Start Screen");
            startScreen.SetActive(false);
        }
        settingUpGame = true;
        levelImage = GameObject.Find("Level Image");
        levelText = GameObject.Find("Level Text").GetComponent<Text>();
        levelText.text = "Day " + currentLevel;
        levelImage.SetActive(true);
        enemies.Clear();
        boardController.SetupLevel(currentLevel);
        Invoke("DisableLevelImage", secondsUntilLevelStart);
    }

    private void DisableStartScreen()
    {
        startScreen.SetActive(false);
        InitializeGame();
    }

    private void DisableLevelImage()
    {
        levelImage.SetActive(false);
        settingUpGame = false;
        isPlayerTurn = true;
        areEnemiesmoving = false;
    }

    private void OnLevelWasLoaded(int levelLoaded)
    {
        currentLevel++;
        InitializeGame();
    }
	
	void Update () {

        if(isPlayerTurn || areEnemiesmoving || settingUpGame)
        {
            return;
        }

        StartCoroutine(MoveEnemies());
	}

    private IEnumerator MoveEnemies()
    {
        areEnemiesmoving = true;

        yield return new WaitForSeconds(0.2f);

        foreach(Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }

        areEnemiesmoving = false;
        isPlayerTurn = true;
    }

    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    public void GameOver()
    {
        isPlayerTurn = false;
        SoundController.Instance.music.Stop();
        SoundController.Instance.PlaySingle(gameOverSound);
        levelText.text = "You starved after " + currentLevel + " days...";
        levelImage.SetActive(true);
        enabled = false; 
    }
}
